package com.atlassian.confluence.test.support.karma;

import com.google.common.collect.ImmutableSet;
import com.google.template.soy.data.SoyData;
import com.google.template.soy.jssrc.restricted.JsExpr;
import com.google.template.soy.jssrc.restricted.SoyJsSrcFunction;
import com.google.template.soy.tofu.restricted.SoyTofuFunction;

import java.util.List;
import java.util.Set;

public class FriendlyFormatDateTimeFunction implements SoyJsSrcFunction, SoyTofuFunction
{
    @Override
    public JsExpr computeForJsSrc(List<JsExpr> args)
    {
        return new JsExpr("AJS.DateTimeFormatting.friendlyFormatDateTime(new Date(" + args.get(0).getText() +
        "), new Date(), Number(AJS.Meta.get('user-timezone-offset')))", 0);
    }

    @Override
    public SoyData computeForTofu(List<SoyData> soyDatas)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getName()
    {
        return "friendlyFormatDateTime";
    }

    @Override
    public Set<Integer> getValidArgsSizes()
    {
        return ImmutableSet.of(1);
    }
}
