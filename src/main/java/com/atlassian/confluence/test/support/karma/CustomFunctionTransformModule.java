package com.atlassian.confluence.test.support.karma;

import com.atlassian.maven.plugins.soytohtml.core.AuiPluginModule;
import com.atlassian.soy.impl.functions.ConcatFunction;

import com.google.inject.multibindings.Multibinder;
import com.google.template.soy.shared.restricted.SoyFunction;

public class CustomFunctionTransformModule extends AuiPluginModule
{

    public CustomFunctionTransformModule(final String propertiesFileLocation)
    {
        super(propertiesFileLocation);
    }

    @Override
    protected void bindFunctions(final Multibinder<SoyFunction> binder)
    {
        super.bindFunctions(binder);
        binder.addBinding().to(ConcatFunction.class);
        binder.addBinding().to(ContextPathFunction.class);
        binder.addBinding().to(DarkFeatureFunction.class);
        binder.addBinding().to(InlineCommentDarkFeatureFunction.class);
        binder.addBinding().to(FriendlyFormatDateTimeFunction.class);
        binder.addBinding().to(MockDocLinkFunction.class);
        binder.addBinding().to(MockHelpUrlFunction.class);
    }
}
